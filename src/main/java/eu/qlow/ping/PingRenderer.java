package eu.qlow.ping;

import net.labymod.core.LabyModCore;
import net.labymod.main.LabyMod;
import net.labymod.user.User;
import net.labymod.utils.ModColor;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.scoreboard.ScoreObjective;
import net.minecraft.scoreboard.Scoreboard;
import org.lwjgl.opengl.GL11;

/**
 * @author Jan
 */
public class PingRenderer {

    private RenderManager renderManager;

    public PingRenderer() {
        this.renderManager = Minecraft.getMinecraft().getRenderManager();
    }

    public void render( PingAddon pingAddon, Entity entity, double x, double y, double z ) {
        if ( !(entity instanceof EntityPlayer) )
            return;

        // Checking whether the addon is enabled
        if ( !pingAddon.isEnabled() )
            return;

        EntityPlayer entityplayer = ( EntityPlayer ) entity;

        // Checking whether the player is sneaking or invisible
        if ( entityplayer.isSneaking() || entityplayer.isInvisible() )
            return;

        // Checking whether the player can even see the entity
        if ( LabyModCore.getMinecraft().getPlayer().equals( entityplayer ) || !LabyModCore.getMinecraft().getPlayer().canEntityBeSeen( entityplayer ) )
            return;

        // Getting the distance to the entity
        double distance = getDistanceSq( entity, LabyModCore.getMinecraft().getPlayer() );

        // Getting the player's user object
        User user = LabyMod.getInstance().getUserManager().getUser( entity.getUniqueID() );

        // Getting the max name tag height
        float maxNameTagHeight = user == null || !LabyMod.getSettings().cosmetics ? 0 : user.getMaxNameTagHeight();

        // Getting the rank that is displayed
        String displayRank = user == null || !user.getRank().isRender() || !user.isRankVisible() ? null : user.getRank().buildTag();


        // Adding the default nametag height
        y += ( double ) (LabyMod.getInstance().getDrawUtils().getFontRenderer().FONT_HEIGHT * 1.15F * 0.02666667F);

        // Add cosmetic height
        y += maxNameTagHeight;

        // Render player tag
        if ( distance < 100.0D ) {
            Scoreboard scoreboard = LabyModCore.getMinecraft().getWorld().getScoreboard();
            ScoreObjective scoreobjective = scoreboard.getObjectiveInDisplaySlot( 2 );

            // Render scoreboard tag
            if ( scoreobjective != null )
                y += ( double ) (LabyMod.getInstance().getDrawUtils().getFontRenderer().FONT_HEIGHT * 1.15F * 0.02666667F);

            if ( displayRank != null )
                y += 0.13f;
        }

        // Checking whether DMI is enabled
        if ( pingAddon.getDamageIndicatorsWrapper().isDamageIndicatorEnabled()
                && distance <= pingAddon.getDamageIndicatorsWrapper().getMaxDistance()
                && !Minecraft.getMinecraft().gameSettings.hideGUI ) {
            y += ( double ) (LabyMod.getInstance().getDrawUtils().getFontRenderer().FONT_HEIGHT * 1.15F * 0.02666667F);
        }

        // Rendering the label
        NetworkPlayerInfo playerInfo = LabyModCore.getMinecraft().getConnection().getPlayerInfo( entityplayer.getUniqueID() );
        int ping = playerInfo != null ? playerInfo.getResponseTime() : -1;

        // Checking whether the ping is known
        if ( ping > 0 ) {
            String pingColor = null;

            // Determining the ping color
            if ( ping < 80 )
                pingColor = "a";
            else if ( ping < 120 )
                pingColor = "e";
            else if ( ping < 250 )
                pingColor = "c";
            else
                pingColor = "4";

            // Rendering the ping
            renderLivingLabel( entity, ModColor.cl( pingColor ) + ping + "ms", x, y, z );
        }
    }

    private void renderLivingLabel( Entity entity, String label, double x, double y, double z ) {
        FontRenderer fontrenderer = renderManager.getFontRenderer();
        float f = 1.6F;
        float f1 = 0.016666668F * f;
        GlStateManager.pushMatrix();
        GlStateManager.translate( ( float ) x + 0.0F, ( float ) y + entity.height + 0.5F, ( float ) z );
        GL11.glNormal3f( 0.0F, 1.0F, 0.0F );
        GlStateManager.rotate( -this.renderManager.playerViewY, 0.0F, 1.0F, 0.0F );
        GlStateManager.rotate( this.renderManager.playerViewX, 1.0F, 0.0F, 0.0F );
        GlStateManager.scale( -f1, -f1, f1 );
        GlStateManager.disableLighting();
        GlStateManager.depthMask( false );
        GlStateManager.disableDepth();
        GlStateManager.enableBlend();
        GlStateManager.tryBlendFuncSeparate( 770, 771, 1, 0 );
        Tessellator tessellator = Tessellator.getInstance();
        WorldRenderer worldrenderer = tessellator.getWorldRenderer();
        int i = 0;

        int j = fontrenderer.getStringWidth( label ) / 2;
        GlStateManager.disableTexture2D();
        worldrenderer.begin( 7, DefaultVertexFormats.POSITION_COLOR );
        worldrenderer.pos( ( double ) (-j - 1), ( double ) (-1 + i), 0.0D ).color( 0.0F, 0.0F, 0.0F, 0.25F ).endVertex();
        worldrenderer.pos( ( double ) (-j - 1), ( double ) (8 + i), 0.0D ).color( 0.0F, 0.0F, 0.0F, 0.25F ).endVertex();
        worldrenderer.pos( ( double ) (j + 1), ( double ) (8 + i), 0.0D ).color( 0.0F, 0.0F, 0.0F, 0.25F ).endVertex();
        worldrenderer.pos( ( double ) (j + 1), ( double ) (-1 + i), 0.0D ).color( 0.0F, 0.0F, 0.0F, 0.25F ).endVertex();
        tessellator.draw();
        GlStateManager.enableTexture2D();
        fontrenderer.drawString( label, -fontrenderer.getStringWidth( label ) / 2, i, 553648127 );
        GlStateManager.enableDepth();
        GlStateManager.depthMask( true );
        fontrenderer.drawString( label, -fontrenderer.getStringWidth( label ) / 2, i, -1 );
        GlStateManager.enableLighting();
        GlStateManager.disableBlend();
        GlStateManager.color( 1.0F, 1.0F, 1.0F, 1.0F );
        GlStateManager.popMatrix();
    }

    public double getDistanceSq( Entity entity, Entity entity2 ) {
        return distanceSq( entity, entity2.posX, entity2.posY, entity2.posZ );
    }

    private double distanceSq( Entity entityPos, double toX, double toY, double toZ ) {
        double d0 = entityPos.posX - toX;
        double d1 = entityPos.posY - toY;
        double d2 = entityPos.posZ - toZ;
        return d0 * d0 + d1 * d1 + d2 * d2;
    }

}
