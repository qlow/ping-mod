package eu.qlow.ping;

import lombok.Getter;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author Jan
 */
public class DamageIndicatorsWrapper {

    @Getter
    private boolean damageIndicatorEnabled;

    @Getter
    private int maxDistance;

    private long lastDamageIndicatorUpdate;

    private boolean notFoundDamageIndicator;
    private int damageIndicatorTries = 0;

    private Class<?> damageIndicatorClass;
    private Method getInstanceMethod;
    private Method isVisibleMethod;
    private Method getDistanceMethod;
    private Field allowedField;

    @SubscribeEvent
    public void onTick( TickEvent.ClientTickEvent event ) {
        long currentTimestamp = System.currentTimeMillis();

        if ( (currentTimestamp - lastDamageIndicatorUpdate) >= 2000L ) {
            updateDamageIndicatorsState();
            this.lastDamageIndicatorUpdate = currentTimestamp;
        }
    }

    /**
     * Updates the damage indicators state
     */
    public void updateDamageIndicatorsState() {
        // Checking whether the mod wasn't found after 10 tries
        if ( this.notFoundDamageIndicator )
            return;

        // Checking whether the class wasn't found until now
        if ( damageIndicatorClass == null ) {
            try {
                // Getting the class
                this.damageIndicatorClass = Class.forName( "net.labymod.addons.damageindicator.DamageIndicator" );
            } catch ( ClassNotFoundException ex ) {
                // If a ClassNotFoundException was thrown we increase the tries and we set the notFoundDamageIndicator boolean at 10 tries
                if ( ++damageIndicatorTries >= 10 ) {
                    this.notFoundDamageIndicator = true;
                }

                return;
            }
        }

        if ( this.getInstanceMethod == null ) {
            try {
                this.getInstanceMethod = damageIndicatorClass.getDeclaredMethod( "getInstance" );
                getInstanceMethod.setAccessible( true );
            } catch ( NoSuchMethodException e ) {
                e.printStackTrace();
            }
        }

        if ( this.isVisibleMethod == null ) {
            try {
                this.isVisibleMethod = damageIndicatorClass.getDeclaredMethod( "isVisible" );
                isVisibleMethod.setAccessible( true );
            } catch ( NoSuchMethodException e ) {
                e.printStackTrace();
            }
        }

        if ( this.getDistanceMethod == null ) {
            try {
                this.getDistanceMethod = damageIndicatorClass.getDeclaredMethod( "getDistance" );
                getDistanceMethod.setAccessible( true );
            } catch ( NoSuchMethodException e ) {
                e.printStackTrace();
            }
        }

        if ( this.allowedField == null ) {
            try {
                this.allowedField = damageIndicatorClass.getDeclaredField( "allowed" );
                allowedField.setAccessible( true );
            } catch ( NoSuchFieldException e ) {
                e.printStackTrace();
            }
        }

        // Checking whether a field or method is still unknown
        if ( getInstanceMethod == null || isVisibleMethod == null || getDistanceMethod == null || allowedField == null )
            return;

        // Now working with the values
        try {
            Object instance = getInstanceMethod.invoke( null );

            if ( instance == null )
                return;

            this.damageIndicatorEnabled = ( Boolean ) isVisibleMethod.invoke( instance ) && ( Boolean ) allowedField.get( instance );

            int distance = ( Integer ) getDistanceMethod.invoke( instance );
            this.maxDistance = distance * distance;
        } catch ( IllegalAccessException e ) {
            e.printStackTrace();
        } catch ( InvocationTargetException e ) {
            e.printStackTrace();
        }
    }

}
