package eu.qlow.ping;

import lombok.Getter;
import net.labymod.api.LabyModAddon;
import net.labymod.api.events.RenderEntityEvent;
import net.labymod.settings.elements.BooleanElement;
import net.labymod.settings.elements.ControlElement;
import net.labymod.settings.elements.SettingsElement;
import net.labymod.utils.Material;
import net.minecraft.entity.Entity;

import java.util.List;

/**
 * @author Jan
 */
public class PingAddon extends LabyModAddon {

    @Getter
    private boolean enabled;

    @Getter
    private DamageIndicatorsWrapper damageIndicatorsWrapper;

    @Getter
    private PingRenderer pingRenderer;

    @Override
    public void onEnable() {
        // Initializing the DMI wrapper
        this.damageIndicatorsWrapper = new DamageIndicatorsWrapper();

        // Initializing the PingRenderer
        this.pingRenderer = new PingRenderer();

        // Registering listeners
        getApi().registerForgeListener( damageIndicatorsWrapper );

        // Registering the RenderEntityListener
        getApi().getEventManager().register( new RenderEntityEvent() {
            @Override
            public void onRender( Entity entity, double x, double y, double z, float partialTicks ) {
                pingRenderer.render( PingAddon.this, entity, x, y, z );
            }
        } );
    }

    @Override
    public void onDisable() {

    }

    @Override
    public void loadConfig() {
        this.enabled = !getConfig().has( "enabled" ) || getConfig().get( "enabled" ).getAsBoolean();
    }

    @Override
    protected void fillSettings( List<SettingsElement> subSettings ) {
        subSettings.clear();
        subSettings.add( new BooleanElement( "Enabled", this, new ControlElement.IconData( Material.LEVER ), "enabled", true ) );
    }

}
